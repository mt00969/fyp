# FYP - Simply Sweet By Asifa

## Pre-requisites
Make sure you have Python installed (https://www.python.org/downloads/)

## Starting the application
Within the root directory (FinalYearProject) run the following commands on the Terminal/CMD:

### `python -m pip install django`

### `pip install -r ./requirements.txt`

### `pip install django-filter`

### `pip install django-multiselectfield`

### `pip install Pillow`

### `pip install virtualenv`

### `python -m virtualenv`

### `python manage.py runserver`
Open http://127.0.0.1:8000/ on your browser to view the web application 

## Test the application
Username: zain
Password: I41717655p