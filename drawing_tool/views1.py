from django.shortcuts import render

# Create your views here.

def canvas(request):
    return render(request,'drawing_tool/templates/drawing_tool/drawing.html')