from django.apps import AppConfig


class DrawingToolConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'drawing_tool'
