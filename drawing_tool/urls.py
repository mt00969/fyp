from django.urls import path, include

from . import views

urlpatterns = [
	path('canvas/', views.canvas, name="canvas"),
]

