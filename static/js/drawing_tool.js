const drawing_board = document.getElementById("drawing-board");

const canvas = new fabric.Canvas('canvas', {
    selection: false,
    backgroundColor: "white",
    width: drawing_board.clientWidth,
    height: drawing_board.clientHeight,
});
canvas.renderAll();

const modes = {
    drawing: 'drawing',
    sprinkles: 'sprinkles'
}

let currentMode;
let mousePressed = false;
let color = 'black';
let brush_width = 5;
let shape_width = 150;
let shape_height = 150;

const color_option = document.querySelectorAll(".color_option");
const selector = document.querySelector(".selector");
const color_picker = document.getElementById("color-picker");
const size_slider = document.getElementById("size-slider");
const fill_color = document.getElementById("fill-color");
const reader = new FileReader();

function change_shape_size(element){
    if(element.value == 'small'){
        shape_width = 150;
        shape_height = 150;
    }else if(element.value == 'medium'){
        shape_width = 200;
        shape_height = 200;
    }else{
        shape_width = 250;
        shape_height = 250;
    }
}

function change_width(value) {
    brush_width = value;
    if (currentMode == modes.drawing) {
        set_drawing_mode(modes.drawing);
    }else if (currentMode == modes.sprinkles){
        set_drawing_mode(modes.sprinkles);
    }
}

function unSelectColors() {
    for (let i = 0; i < color_option.length; i++) {
        color_option[i].classList.remove('selected');
    }
    selector.classList.remove('selected');
}

function select_color(color_name, e) {
    color = color_name;
    unSelectColors();
    e.classList.add('selected');
    if (currentMode == modes.drawing) {
        set_drawing_mode(modes.drawing);
    }else if (currentMode == modes.sprinkles){
        set_drawing_mode(modes.sprinkles);
    }
}

color_picker.addEventListener("change", function () {
    color = document.getElementById("color-picker").value;
    if (currentMode == modes.drawing) {
        set_drawing_mode(modes.drawing);
    }else if (currentMode == modes.sprinkles){
        set_drawing_mode(modes.sprinkles);
    }
});

function color_selector(e) {
    unSelectColors();
    e.classList.add('selected');
    color = document.getElementById("color-picker").value;
    if (currentMode == modes.drawing) {
        set_drawing_mode(modes.drawing);
    }else if (currentMode == modes.sprinkles){
        set_drawing_mode(modes.sprinkles);
    }
}

function set_drawing_mode(mode) {
    if(mode === modes.drawing){
        currentMode = modes.drawing;
        canvas.freeDrawingBrush = new fabric.PencilBrush(canvas);
        canvas.freeDrawingBrush.color = color;
        canvas.isDrawingMode = true;
        canvas.freeDrawingBrush.width = parseInt(brush_width);
        canvas.renderAll();
    }else{
        console.log("else");
        currentMode = modes.sprinkles;
        canvas.freeDrawingBrush = new fabric.SprayBrush(canvas);
        canvas.freeDrawingBrush.color = color;
        canvas.freeDrawingBrush.density = 5;
        canvas.isDrawingMode = true;
        canvas.freeDrawingBrush.width = 40;
        canvas.renderAll();
    }
}

function clear_canvas() {
    canvas.getObjects().forEach((o) => {
        if (o !== canvas.backgroundColor) {
            canvas.remove(o)
        }
    })
}

function undo() {
    let canvas_obj = canvas.getObjects();
    canvas.remove(canvas_obj[canvas_obj.length - 1]);
}

function createRect() {
    const canvasCenter = canvas.getCenter();
    
    if (fill_color.checked == false) {
        const rect = new fabric.Rect({
            width: shape_width,
            height: shape_height,
            left: canvasCenter.left,
            top: (shape_height/2)+parseInt(brush_width),
            originX: 'center',
            originY: 'center',
            fill: 'transparent',
            stroke: color,
            strokeWidth: parseInt(brush_width),
        });
        canvas.add(rect);
        canvas.renderAll();
    } else {
        const rect = new fabric.Rect({
            width: shape_width,
            height: shape_height,
            left: canvasCenter.left,
            top: (shape_height/2)+parseInt(brush_width),
            originX: 'center',
            originY: 'center',
            fill: color,
        });
        canvas.add(rect);
        canvas.renderAll();
    }
}

function createCircle() {
    const canvasCenter = canvas.getCenter();
    if (fill_color.checked == false) {
        const circle = new fabric.Circle({
            radius: shape_width/2,
            left: canvasCenter.left,
            top: (shape_height/2)+parseInt(brush_width),
            originX: 'center',
            originY: 'center',
            fill: 'transparent',
            stroke: color,
            strokeWidth: parseInt(brush_width),
        });
        canvas.add(circle);
        canvas.renderAll();
    } else {
        const circle = new fabric.Circle({
            radius: shape_width/2,
            left: canvasCenter.left,
            top: (shape_height/2)+parseInt(brush_width),
            originX: 'center',
            originY: 'center',
            fill: color,
        });
        canvas.add(circle);
        canvas.renderAll();
    }
}

function createTriangle() {
    const canvasCenter = canvas.getCenter();
    if (fill_color.checked == false) {
        const triangle = new fabric.Triangle({
            width: shape_width,
            height: shape_height,
            left: canvasCenter.left,
            top: (shape_height/2)+parseInt(brush_width),
            originX: 'center',
            originY: 'center',
            fill: 'transparent',
            stroke: color,
            strokeWidth: parseInt(brush_width),
        });
        canvas.add(triangle);
        canvas.renderAll();
    } else {
        const triangle = new fabric.Triangle({
            width: shape_width,
            height: shape_height,
            left: canvasCenter.left,
            top: (shape_height/2)+parseInt(brush_width),
            originX: 'center',
            originY: 'center',
            fill: color,
        });
        canvas.add(triangle);
        canvas.renderAll();
    }
}

const toggleMode = (mode, event) => {
    if(mode === modes.drawing){
        if (currentMode === modes.drawing) {
            currentMode = '';
            canvas.isDrawingMode = false;
            canvas.renderAll();
            event.classList.remove('active');
        } else {
            event.classList.add('active');
            set_drawing_mode(modes.drawing);
        }

        document.getElementById("spray").classList.remove('active');
    }
    if(mode === modes.sprinkles){
        if (currentMode === modes.sprinkles) {
            currentMode = '';
            canvas.isDrawingMode = false;
            canvas.renderAll();
            event.classList.remove('active');
        } else {
            event.classList.add('active');
            set_drawing_mode(modes.sprinkles)
        }
        document.getElementById("brush").classList.remove('active');
    }

}

const imgAdded = (e) => {
    const inputElem = document.getElementById('myImg')
    const file = inputElem.files[0];
    reader.readAsDataURL(file)
}

const inputFile = document.getElementById('myImg');
inputFile.addEventListener('change', imgAdded)

reader.addEventListener("load", () => {
    fabric.Image.fromURL(reader.result, img => {
        canvas.add(img)
        canvas.requestRenderAll()
    })
})

function createCat() {
    fabric.Image.fromURL('../images/icons/cat.png', function (img) {
        var oImg = img.set({ left: 0, top: 0 }).scale(0.25);
        canvas.add(oImg);
    });
}

function createDog() {
    fabric.Image.fromURL('../images/icons/dog.png', function (img) {
        var oImg = img.set({ left: 0, top: 0 }).scale(0.25);
        canvas.add(oImg);
    });
}

function createFlower(path) {
    fabric.Image.fromURL(path, function (img) {
        var oImg = img.set({ left: 0, top: 0 }).scale(0.25);
        canvas.add(oImg);
    });
}

canvas.on('mouse:over', (event) => {
    if (mousePressed && currentMode == modes.drawing) {
        canvas.isDrawingMode = true;
        canvas.renderAll();
    }
})

canvas.on('mouse:down', (event) => {
    mousePressed = true;
})

canvas.on('mouse:up', (event) => {
    mousePressed = false;
})

function download() {
    var dataURL = canvas.toDataURL('image/png');

    // Create a link element
    var link = document.createElement('a');
    link.download = 'canvas.png';
    link.href = dataURL;

    // Click the link to download the image
    link.click();
}

function delete_object(){
    canvas.remove(canvas.getActiveObject());
}